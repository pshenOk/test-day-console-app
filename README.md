# README #

1. Create bitbucket repository with package.json like this https://bitbucket.org/pshenOk/test-day/src/master/
2. Add some dependency, for example 'lodash'
3. Pull this repository https://bitbucket.org/pshenOk/test-day-console-app/src/master/
4. Run `npm install` or `yarn install`
5. Add .env file or ENV variable `BITBUCKET_TOKEN=<your basic auth token>`
6. Run the script as shown in the example use your own parameters -> `node src/index.js --package=lodash --v="4.17.14" --repo=test-day --workspace=pshenOk --author="pshenOk <k.pshenychnyy@gmail.com>"`

Script will change the version of lodash, create a branch 'updates/lodash@4.17.14' and pull request for this branch