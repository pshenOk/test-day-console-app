require('dotenv').config();

module.exports = {
	bitbucket: {
		token:    process.env.BITBUCKET_TOKEN,
	}
}