const rp = require('request-promise-native');
const config = require('../app/config');

class BitbucketSDK {

	constructor({ auth }) {
		this.token = auth.token;
	}

	async getFileData({ workspace, repo, filename }) {
		try {
			const stringData = await rp({
				method: 'GET',
				url: `https://api.bitbucket.org/2.0/repositories/${workspace}/${repo}/commits/master`,
				headers: {
					Authorization: `Basic ${this.token}`,
				}
			});
			const data = JSON.parse(stringData);

			const stringFileData = await rp({
				method: 'GET',
				url: `https://api.bitbucket.org/2.0/repositories/${workspace}/${repo}/src/${data.values[0].hash}/${filename}`,
				headers: {
					Authorization: `Basic ${this.token}`,
				}
			});
			const fileData = JSON.parse(stringFileData);
			return fileData;
		} catch (error) {
			throw new Error('Bitbucket API error: \n', error);
		}
	}

	async createBranch ({workspace, repo, data}) {
		try {
			await rp({
				method: 'POST',
				url:    `https://api.bitbucket.org/2.0/repositories/${workspace}/${repo}/src`,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					Authorization: `Basic ${this.token}`,
				},
				form: {
					'message': data.message,
					'author': data.author,
					'branch': data.branch,
					'/package.json': data.fileData,
				}
			});

			return data.branch;
		} catch (error) {
			throw new Error('Bitbucket API error: \n', error);
		}
	}

	async createPullRequest ({workspace, repo, data}) {
		try {
			const result = await rp({
				method: 'POST',
  				url: `https://api.bitbucket.org/2.0/repositories/${workspace}/${repo}/pullrequests`,
  				headers: {
    				'Content-Type': 'application/json',
					Authorization: `Basic ${this.token}`,
  				},
  				body: JSON.stringify({
					title: data.title,
					source: {
						branch: {
							name: data.branch
						}
					}
				})
			});

			return data.title;
		} catch (error) {
			throw new Error('Bitbucket API error: \n', error);
		}
	}
};

module.exports = new BitbucketSDK({
	baseUrl: 'https://api.bitbucket.org/2.0',
	auth: {
		token: config.bitbucket.token,
	}
});