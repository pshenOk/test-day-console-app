const bitbucket = require('./infra/Bitbucket');
const { argv } = require('yargs');

const PACKAGE_FOR_UPGRADE = argv.package;
const PACKAGE_VERSION = argv.v;
const BITBUCKET_REPOSITORY = argv.repo;
const BITBUCKET_WORKSPACE = argv.workspace;
const AUTHOR = argv.author; // pshenOk <k.pshenychnyy@gmail.com>


(async ({ package, version, repo, workspace, author }) => {
	
	try {
		let packageJSON = await bitbucket.getFileData({
			repo:      repo,
			workspace: workspace,
			filename:  'package.json',
		});

		packageJSON.dependencies[package] = version;
		console.log(`✅ File package.json updated`);

		const branch = await bitbucket.createBranch({
			repo:      repo,
			workspace: workspace,
			data: {
				message:  `${package} version upgrade to ${version}`,
				author:   author,
				branch:   `updates/${package}@${version}`,
				fileData: JSON.stringify(packageJSON),
			}
		});
		console.log(`✅ Branch -> ${branch} created`);

		const result = await bitbucket.createPullRequest({
			repo:      repo,
			workspace: workspace,
			data: {
				title: `Pull request for branch ${branch}`,
				branch: branch,
			}
		});
		console.log(`✅ ${result} created`);


	} catch (error) {
		console.error(error)
	}

})({ 
	package:   PACKAGE_FOR_UPGRADE,
	version:   PACKAGE_VERSION,
	repo:      BITBUCKET_REPOSITORY,
	workspace: BITBUCKET_WORKSPACE,
	author:    AUTHOR,
});